var chai = require('chai');
var expect = chai.expect;
var app = require('../src/app.js');

describe('Application test', function() {
  it('Fifth version', function() {
    const result = app.render(
      {
        "p.my-class#my-id": "hello",
        "p.my-class1.my-class2": "example<a>asd</a>",
        "p.my-class3#my-id2": [
          {
            "span.my-class4#my-id4": "test nested"
          }
        ]
      }
    );
    expect(result).to.equal('<p class="my-class" id="my-id">hello</p><p class="my-class1 my-class2">example&lt;a&gt;asd&lt;/a&gt;</p><p class="my-class3" id="my-id2"><ul><li><span class="my-class4" id="my-id4">test nested</span></li></ul></p>');
  });
});
