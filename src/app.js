const fs = require('fs');

// Escape HTML special characters
const escape = (html) => html.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');

// Render tag by CSS selector and inner text
const renderTag = (selector, value) => {
  var classes = [],
      id,
      tagName,
      splitted = selector.split(/(?=\.)|(?=#)|(?=\[)/);
  splitted.forEach((t) => {
    if(t.charAt(0) == '.') {
      classes.push(t.substring(1))
    } else if(t.charAt(0)== '#') {
      id = t.substring(1);
    } else {
      tagName = t;
    }
  });
  return `<${tagName}`
    + (classes.length ? ` class="${classes.join(' ')}"` : '')
    + (id ? ` id="${id}"` : '')
    + '>'
    + (typeof value === 'string' ? escape(value) : render(value))
    + `</${tagName}>`;
}

// Render HTML
const render = (el) =>
  Array.isArray(el) ?
    '<ul>'+el.reduce((prev, el) => prev+'<li>'+render(el)+'</li>', '')+'</ul>'
  : Object.keys(el).reduce(
    (prev, tag) => prev+renderTag(tag, el[tag])
  , '');

// Read file and run rendering
fs.readFile('./src/source.json', 'utf8', (e, d) => console.log(e ? e : render(JSON.parse(d))));

// Export render method for tests
module.exports = { render };
